package com.company.SecondTask;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;

import static java.lang.Math.*;

/**
 * 2.1.  Необходимо посчитать площадь круга с указанным радиусом с точностью 50 знаков после запятой
 * 2.2. Даны три числа, например, 0.1, 0.15 и 0.25. Числа даны в виде строки. Необходимо ответить,
 * является ли третье число суммой двух первых. * Учесть локаль пользователя и разделитель целой-дробной частей в данных строках
 * 2.3. Даны три числа. Нужно найти минимум и максимум не используя условный и тернарный операторы
 */


public class MathCalc {

    public BigDecimal countCircleArea(String stringAreaValue) {
        BigDecimal bigDecimalAreaValue = new BigDecimal(stringAreaValue);
        BigDecimal circleArea = bigDecimalAreaValue;
        BigDecimal PI = new BigDecimal(Math.PI);
        circleArea = circleArea.multiply(circleArea).multiply(PI);

        return circleArea.setScale(50, RoundingMode.HALF_UP);

    }

    public boolean isThirdNumberTheSumOfFirstSecond(String firstString, String secondString, String thirdString) {
        Double first = Double.parseDouble(firstString);
        Double second = Double.parseDouble(secondString);
        Double third = Double.parseDouble(thirdString);

        first += second;
        int result = first.compareTo(third);
        return result == 0 ? true : false;

    }

    public void findMinMaxfromThreeNumbers(int first, int second, int third) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(first);
        arrayList.add(second);
        arrayList.add(third);

        System.out.println("Maximum of three numbers is " + Collections.max(arrayList));
        System.out.println("Minimum one is " + Collections.min(arrayList));

    }

}
