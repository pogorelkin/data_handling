package com.company.SecondTask;

public class DemoService {
    public void startDemo() {
        MathCalc mathCalc = new MathCalc();

        String circleRadius = "20.21";
        System.out.println("Circle radius is equal to " + circleRadius);
        System.out.println("Area of circle is equal to " + mathCalc.countCircleArea(circleRadius).toString());

        String firstValue = "20.1";
        String secondValue = "12.1";
        String thirdValue = "30.2";
        System.out.println("These are three numbers: " + " " + firstValue + ", " + secondValue + ", " + thirdValue);

        System.out.println("The Third number is equal to sum of the first and the second ones. " +
                mathCalc.isThirdNumberTheSumOfFirstSecond(firstValue, secondValue, thirdValue));

        int firstInt = 1;
        int secondInt = 2;
        int thirdInt = 3;
        System.out.printf("Three ints: %d, %d, %d. \n", firstInt, secondInt, thirdInt);
        mathCalc.findMinMaxfromThreeNumbers(firstInt, secondInt, thirdInt);

    }

}
