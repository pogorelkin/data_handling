package com.company.FirstTask;
/**
 * 1.1.  Определить ваш возраст с момента рождения
 * (можно нафантазировать, если нет точных данных =) ) на момент запуска программы.
 * Возраст в секундах, минутах, часах, днях, месяцах и годах
 * 1.2.  Даны две даты в виде "25.07.1921", т.е.
 * "день месяца 2 разряда, месяц 2 разряда, год 4 разряда".
 * Найти разницу в днях между ними. Число должно быть всегда положительным
 * 1.3. Дана строка вида "Friday, Aug 10, 2016 12:10:56 PM".
 * Необходимо сконвертировать ее в вид “2018-08-10".
 */

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static java.lang.Math.abs;

import java.time.temporal.ChronoUnit;
import java.util.Locale;


public class DateCalctulation {
    private LocalDateTime birthdate;

    public DateCalctulation(LocalDateTime birthdate) {
        this.birthdate = birthdate;
    }

    public DateCalctulation() {

    }

    public DateCalctulation(int years, int months, int days, int hours, int minutes, int seconds) {
        birthdate = LocalDateTime.of(years, months, days, hours, minutes, seconds);
    }

    public void setBirthdate(LocalDateTime birthdate) {
        this.birthdate = birthdate;
    }

    public void getMyAge() {
        LocalDateTime birthday = this.birthdate;
        LocalDateTime today = LocalDateTime.now();
        System.out.println("Today is " + today);
        System.out.print("My age is " + today.minusYears(birthday.getYear()).getYear() + " years, ");
        System.out.print(abs(today.minusMonths(birthdate.getMonthValue()).getMonthValue()) + " months, ");
        System.out.print(abs(today.minusDays(birthdate.getDayOfMonth()).getDayOfMonth()) + " days, ");
        System.out.print(today.minusHours(birthdate.getHour()).getHour() + " hours, ");
        System.out.print(today.minusMinutes(birthdate.getMinute()).getMinute() + " minutes, ");
        System.out.println(today.minusSeconds(birthdate.getSecond()).getSecond() + " seconds.");


    }


    public long calculateDaysBetweenDates(String firstDate, String secondDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate first = LocalDate.parse(firstDate, formatter);
        LocalDate second = LocalDate.parse(secondDate, formatter);
        long daysBetween = ChronoUnit.DAYS.between(first, second);

        return abs(daysBetween);
    }


    public String convertDateType(String date) {
        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("EEEE, MMM d, yyyy HH:mm:ss a", Locale.ENGLISH);
        DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime dateTime = LocalDateTime.parse(date, inputFormat);

        return dateTime.format(outputFormat);

    }


    @Override
    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy MM dd  HH:mm:ss");
        return birthdate.format(formatter);
    }

}
