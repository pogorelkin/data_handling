package com.company.FirstTask;

import java.time.LocalDateTime;

public class DemoService {
    public void startDemo(){
        DateCalctulation dateCalctulation = new DateCalctulation();
        LocalDateTime birthday = LocalDateTime.of(1997, 02, 28, 22, 50, 58);
        System.out.println("Birthday is " + birthday.toString());
        dateCalctulation.setBirthdate(birthday);
        dateCalctulation.getMyAge();

        String date1 = "11.02.1990";
        String date2 = "11.10.1990";
        System.out.println("Number of days between dates " + date1 + " and " + date2 + " is ");
        System.out.println(dateCalctulation.calculateDaysBetweenDates(date1,date2));



        String dateUnformatted = "Friday, Aug 12, 2016 12:10:56 PM";
        System.out.println("This date format: " + dateUnformatted + "to " );
        System.out.println(dateCalctulation.convertDateType(dateUnformatted));
    }
}
